from django.db import models
from django.utils import timezone
from http.cookiejar import DAYS

class Course(models.Model):
    
    course_id = models.AutoField(primary_key=True)
    course_name = models.CharField(max_length = 35)
    credits = models.IntegerField()
    
    def __str__(self):
        return ' '.join([self.course_name])

class Attendance(models.Model):
    
    attendance_id = models.AutoField(primary_key=True)
    student = models.ForeignKey("Student",on_delete=models.CASCADE)
    course = models.ForeignKey("Course",on_delete=models.CASCADE)
    
    def __str__(self):
        return ' '.join([str(self.attendance_id),self.student,str(self.course)])
  
class Student(models.Model):
    
    name_first = models.CharField(max_length = 15)
    name_last = models.CharField(max_length = 15)
    student_id = models.AutoField(primary_key=True)
    date_enroll = models.DateTimeField('The date you enrolled',default=timezone.now)
    email = models.EmailField() 
    attendances = models.ForeignKey("Attendance",related_name="+",on_delete=models.CASCADE)
   
    def __str__(self):
        return ' '.join([self.name_first,self.name_last,str(self.date_enroll),self.email])

class Event(models.Model):
    
    event_name = models.CharField(max_length = 35)
    event_id = models.AutoField(primary_key=True)
    event_description = models.TextField(default="Just put something")
    date_start = models.DateTimeField('The event starts',default=timezone.now)
    date_end = models.DateTimeField('The event ends',default=timezone.now)
    
    def __str__(self):
        return ' '.join([self.event_name,str(self.event_id),self.event_description,str(self.date_start),str(self.date_end)])