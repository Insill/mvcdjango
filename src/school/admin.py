'''
Created on 14.6.2016

@author: Insill
'''

from django.contrib import admin
from .models import Student,Course,Event,Attendance

admin.site.register(Student)
admin.site.register(Course)
admin.site.register(Event)
admin.site.register(Attendance)
    