from django.http import HttpResponse, Http404
from .models import Student, Course, Event
from django.shortcuts import render
from django.template import loader 

def index(request):
    
    list_objects = [] 
    list_objects.append(Student.objects.order_by('-student_id')[:5])
    list_objects.append(Course.objects.order_by('-course_id')[:5])
    list_objects.append(Event.objects.order_by('event_id')[:5])
    template = loader.get_template("school/index.html")
    context = { "list_objects": list_objects,}
    return HttpResponse(template.render(context,request))

def view_student(request,student_id):
    try:
        student = Student.objects.get(pk=student_id)
    except Student.DoesNotExist:
        raise Http404("Student does not exist")
    return render(request,"school/detail.html", {"student": student})

def view_course(request, course_id):
    try:
        course = Course.objects.get(pk=course_id)
    except Course.DoesNotExist:
        raise Http404("Course does not exist")
    return render(request,"school/detail.html", {"course": course})
 
def view_event(request, event_id):
    try:
        event = Event.objects.get(pk=event_id)
    except Event.DoesNotExist:
        raise Http404("Event does not exist")
    return render(request, "school/detail.html", {"event": event})

def results(request, course_id):
    return HttpResponse("Booooooo")

def detail(): 
    return None

