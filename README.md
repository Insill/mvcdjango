# README #
Similarly to MVCSharp, this is an application utilizing the MVC architecture, this time with Python and Django. 
The project's development began in June 2016 and has since continued occasionally. 

## Models ##
### Student ###

* String name_first
* String name_last
* Primary key student_id
* date date_enroll
* email email

### Course ###

* Primary key course_id
* String cours_name
* int credits 

### Attendance ###

* Primary key attendance_id 
* Foreign key Student(student_id)
* Foreign key Course(course_id) 

### Event ###

* String event_name
* Primary key event_id 
* String event_description
* date date_start
* date date_end
